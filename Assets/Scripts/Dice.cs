﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class Dice: MonoBehaviour
{
    public const int DICE_RESET = -1;

    private int DiceNumber = DICE_RESET;
    public Dice() { }
    public void throwDice()
    {
        int FirstRandomNumber = UnityEngine.Random.Range(1, 7);
        int SecondRandomNumber = UnityEngine.Random.Range(1, 7);
        Debug.Log("Dice Thrown: " + FirstRandomNumber + " " + SecondRandomNumber);
        DiceNumber = FirstRandomNumber + SecondRandomNumber;
    }
    
    public int getDiceNumber()
    {
        return DiceNumber;
    }

    public void ResetDice()
    {
        DiceNumber = DICE_RESET;

    }
}

