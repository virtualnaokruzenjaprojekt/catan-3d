using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuSounds : MonoBehaviour
{

    public AudioSource clickSource;
    public AudioSource hoverSource;
    public AudioSource addPlayerSource;
    public AudioSource removePlayerSource;

    public void Quit()
    {
        Application.Quit();
    }

    public void PlayClickSound()
    {
        clickSource.Play();
    }

    public void PlayHoverSound()
    {
        hoverSource.Play();
    }

    public void PlayAddPlayerSound()
    {
        addPlayerSource.Play();
    }

    public void PlayRemovePlayerSound()
    {
        removePlayerSource.Play();
    }
}
