using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuManager : MonoBehaviour
{
    public Canvas overlayCanvas;
    void Start()
    {
        overlayCanvas.worldCamera = Camera.main;
    }
}
