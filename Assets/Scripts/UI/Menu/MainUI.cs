using System.Collections;
using System.Collections.Generic;
using StateMachine;
using StateMachine.States;
using UnityEngine;

public class MainUI : MonoBehaviour
{
    public void CallShop()
    {
        UIManager.instance.OpenShopDialog();
    }

    public void CallTrade()
    {
        UIManager.instance.OpenTradeDialog();
    }

    public void CallMonopoly()
    {
        UIManager.instance.OpenMonopolyDialog();
    }

    public void CallYOP()
    {
        UIManager.instance.OpenYOPDialog();
    }

    public void CallRoadBuilding()
    {
        UIManager.instance.PlayRoadBuilding();
    }

    public void CallKnight()
    {
        UIManager.instance.PlayKnight();
    }

    public void CallVictoryPoint()
    {
        UIManager.instance.PlayVictoryPoint();
    }

    public void CallNextTurn()
    {
        // CALL: next turn
        GameManager.instance.EndPlayerTurn();
    }
}
