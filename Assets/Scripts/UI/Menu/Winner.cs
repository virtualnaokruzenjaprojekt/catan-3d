using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Winner : MonoBehaviour
{

    public TMP_Text winnerText;
    public Canvas overlayCanvas;

    private void Start()
    {
        overlayCanvas.worldCamera = Camera.main;
    }

    public void SetWinner(string text)
    {
        winnerText.text = text;
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
