using System.Collections;
using System.Collections.Generic;
using StateMachine;
using StateMachine.States;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerSetup : MonoBehaviour
{
    public List<string> playerNames = new();
    public Button addPlayerButton;
    public Button startGameButton;
    public List<Transform> namePlaceholders;
    public TMP_InputField inputFieldText;
    public TMP_Text totalPlayersText;
    public MainMenuManager mainMenuManager;

    const int maxPlayers = 4;
    const int minPlayers = 2;
    public int currentPlayers = 0;

    public void AddPlayer()
    {
        string name = inputFieldText.text.Trim().RemoveConsecutiveCharacters(' ');
        if (name.Length == 0 || playerNames.Contains(name))
            return;

        playerNames.Add(name);
        namePlaceholders[currentPlayers].gameObject.GetComponentInChildren<TMP_Text>(true).text = name;
        namePlaceholders[currentPlayers].gameObject.GetComponentInChildren<Button>(true).gameObject.SetActive(true);
        currentPlayers++;

        inputFieldText.text = "";
        totalPlayersText.text = "TOTAL PLAYERS: " + currentPlayers;

        if (currentPlayers == maxPlayers)
        {
            addPlayerButton.interactable = false;
            inputFieldText.enabled = false;
            inputFieldText.placeholder.GetComponent<TMP_Text>().text = "Max player count reached...";
        }

        if (currentPlayers >= minPlayers)
            startGameButton.interactable = true;

    }

    public void RemovePlayer(int playerNumber)
    {
        playerNames.RemoveAt(playerNumber);
        currentPlayers--;

        for (int i = 0; i < currentPlayers; i++)
        {
            namePlaceholders[i].gameObject.GetComponentInChildren<TMP_Text>(true).text = playerNames[i];
            namePlaceholders[i].gameObject.GetComponentInChildren<Button>(true).gameObject.SetActive(true);
        }

        for (int i = currentPlayers; i < maxPlayers; i++)
        {
            namePlaceholders[i].gameObject.GetComponentInChildren<TMP_Text>(true).text = "";
            namePlaceholders[i].gameObject.GetComponentInChildren<Button>(true).gameObject.SetActive(false);
        }

        totalPlayersText.text = "TOTAL PLAYERS: " + currentPlayers;
        addPlayerButton.interactable = true;
        inputFieldText.enabled = true;
        inputFieldText.placeholder.GetComponent<TMP_Text>().text = "Enter player name...";

        if (currentPlayers < minPlayers)
            startGameButton.interactable = false;
    }

    public void StartGame()
    {
        // Call: setup player names
        GameManager.playerNames = playerNames;
        SceneManager.UnloadSceneAsync("Scenes/Menu");
        
        GameManager.instance.ChangeState(new InitializingGame(GameManager.instance));
        Destroy(mainMenuManager.gameObject);
    }

    void OnDisable()
    {
        inputFieldText.text = "";
        playerNames.Clear();
        currentPlayers = 0;
        addPlayerButton.interactable = true;
        inputFieldText.enabled = true;
        inputFieldText.placeholder.GetComponent<TMP_Text>().text = "Enter player name...";
        totalPlayersText.text = "TOTAL PLAYERS: " + currentPlayers;
        for (int i = 0; i < maxPlayers; i++)
        {
            namePlaceholders[i].gameObject.GetComponentInChildren<TMP_Text>(true).text = "";
            namePlaceholders[i].gameObject.GetComponentInChildren<Button>(true).gameObject.SetActive(false);
        }
        startGameButton.interactable = false;
    }
}
