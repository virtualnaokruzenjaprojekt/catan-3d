using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using StateMachine;
using StateMachine.States;

public class ShopManager : MonoBehaviour
{
    public int resource = -1; // owned resource
    public int exchangeResource = -1; // wanted resource
    public Image[] resourceButtonBg; // owned resource button background
    public Image[] exchangeButtonBg; // wanted resource button background
    public Button[] resourceButton; // resource button list to set interactable property
    public Button[] exhangeButton; // wanted button list to set interactable property
    public Button[] build; // button list for buildings and development card interactable property
    public Canvas overlayCanvas;
    public Button confirmButton;
    public Color colorUnselected;
    public Color colorSelected;
    public Player currentPlayer;

    void Start()
    {
        overlayCanvas.worldCamera = Camera.main;

        currentPlayer = GameManager.instance.activePlayer;

        // can build road?
        if (CheckResources(ResourceCardType.Lumber, 1) && CheckResources(ResourceCardType.Brick, 1))
        {
            build[0].interactable = true;
        }

        // can build village?
        if (CheckResources(ResourceCardType.Lumber, 1) && CheckResources(ResourceCardType.Brick, 1) && CheckResources(ResourceCardType.Grain, 1) && CheckResources(ResourceCardType.Wool, 1))
        {
            build[1].interactable = true;
        }

        // can build city?
        if (CheckResources(ResourceCardType.Grain, 2) && CheckResources(ResourceCardType.Ore, 3))
        {
            build[2].interactable = true;
        }

        // can buy development card?
        if (CheckResources(ResourceCardType.Grain, 1) && CheckResources(ResourceCardType.Wool, 1) && CheckResources(ResourceCardType.Ore, 1))
        {
            build[3].interactable = true;
        }

        // development card can be bought?
        if (!Utils.CanBuyDevelopmentCard(GameManager.instance.developmentDeck))
            build[3].interactable = false;

        // can exchange brick?
        if (CheckResources(ResourceCardType.Brick, 4))
        {
            resourceButton[0].interactable = true;
        }

        // can exchange grain?
        if (CheckResources(ResourceCardType.Grain, 4))
        {
            resourceButton[1].interactable = true;
        }

        // can exchange lumber?
        if (CheckResources(ResourceCardType.Lumber, 4))
        {
            resourceButton[2].interactable = true;
        }

        // can exchange ore?
        if (CheckResources(ResourceCardType.Ore, 4))
        {
            resourceButton[3].interactable = true;
        }

        // can exchange wool?
        if (CheckResources(ResourceCardType.Wool, 4))
        {
            resourceButton[4].interactable = true;
        }
    }

    // if player has the amount of this type of resource, return true
    bool CheckResources(ResourceCardType resource, int amount)
    {
        if(!currentPlayer.resources.ContainsKey(resource) || currentPlayer.resources[resource] < amount)
        {
            return false;
        }

        return true;
    }

    // selected building or development card
    public void SelectShopItem(int index)
    {
        GameObject.FindGameObjectWithTag("BoardCamera").GetComponent<FlyCamera>().enabled = true;
        UIManager.instance.BuyFromShop(this, index);

        Destroy(gameObject);
    }

    // selected resource exhange
    public void ConfirmShop()
    {
        if (resource != -1 && exchangeResource != -1)
        {
            // CALL: exhange resource with shop
            GameManager.instance.TradeResourcesWithShop((ResourceCardType) resource, (ResourceCardType) exchangeResource);
        }

        Destroy(gameObject);
    }

    // select resource (< 5) -> owned resource, (>= 5) -> wanted resource
    public void SelectResource(int index)
    {
        if (index >= 5)
        {
            if (exchangeResource != -1)
            {
                exchangeButtonBg[exchangeResource].color = colorUnselected;
            }
            exchangeResource = index - 5;
            exchangeButtonBg[index - 5].color = colorSelected;
        }
        else
        {
            if (resource != -1)
            {
                resourceButtonBg[resource].color = colorUnselected;
            }
            resource = index;
            resourceButtonBg[index].color = colorSelected;
        }

        if (resource != -1 && exchangeResource != -1)
        {
            confirmButton.interactable = true;
        }
    }

    public void CancelShop()
    {
        Destroy(gameObject);
    }
}
