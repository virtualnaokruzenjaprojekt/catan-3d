using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using StateMachine;

public class YearOfPlenty : MonoBehaviour
{
    public int firstResource = -1;
    public int secondResource = -1;
    public Image[] firstButton; // first resource button background
    public Image[] secondButton; // second resource button background
    public Canvas overlayCanvas;
    public Button confirmButton;
    public Color colorUnselected;
    public Color colorSelected;

    void Start()
    {
        overlayCanvas.worldCamera = Camera.main;
    }

    // execute YOP if both resources are selected
    public void ConfirmYearOfPlenty()
    {
        if(firstResource != -1 && secondResource != -1)
        {
            // CALL: handle year of plenty
            UIManager.instance.PlayYOP((ResourceCardType) firstResource, (ResourceCardType) secondResource);
            Destroy(gameObject);
        }
    }

    // select resource (< 5) -> first resource, (>= 5) -> second resource
    public void SelectResource(int index)
    {
        if(index >= 5)
        {
            if (secondResource != -1)
            {
                secondButton[secondResource].color = colorUnselected;
            }
            secondResource = index - 5;
            secondButton[index - 5].color = colorSelected;
        } else
        {
            if (firstResource != -1)
            {
                firstButton[firstResource].color = colorUnselected;
            }
            firstResource = index;
            firstButton[index].color = colorSelected;
        }

        if (firstResource != -1 && secondResource != -1)
        {
            confirmButton.interactable = true;
        }
    }

    public void CancelYearOfPlenty()
    {
        Destroy(gameObject);
    }
}
