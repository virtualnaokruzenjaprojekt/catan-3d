using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using StateMachine;

namespace Trade
{
    public class TradeManager : MonoBehaviour
    {
        public Dictionary<ResourceCardType, int> offering = new() // init offering resource list
        {
            { ResourceCardType.Brick, 0 },
            { ResourceCardType.Grain, 0 },
            { ResourceCardType.Lumber, 0 },
            { ResourceCardType.Ore, 0 },
            { ResourceCardType.Wool, 0 },
        };
        public Dictionary<ResourceCardType, int> receiving = new() // init receiving resource list
        {
            { ResourceCardType.Brick, 0 },
            { ResourceCardType.Grain, 0 },
            { ResourceCardType.Lumber, 0 },
            { ResourceCardType.Ore, 0 },
            { ResourceCardType.Wool, 0 },
        };
        public List<TMP_Text> offerResourceText; // offer resource amount text
        public List<TMP_Text> receiveResourceText; // receive resource amount text
        public List<Button> tradePlayersButtons; // list of player buttons
        public List<Button> offerAddButtons; // list of buttons to set interactable property if player can offer that type of resource
        public Player currentPlayer;
        public Player tradee;
        public List<Player> players;
        public Canvas overlayCanvas;
        public GameObject tradeConfirmDialog; // trade confirm dialog

        public static TradeManager instance = null;

        void Start()
        {

            if (instance == null)
            {
                instance = this;
            } else
            {
                Debug.LogError("ANOTHER TRADE MANAGER INITATED, THIS SOULD NOT HAPPEN");
                Destroy(gameObject);
            }

            overlayCanvas.worldCamera = Camera.main;

            currentPlayer = GameManager.instance.activePlayer;
            players = GameManager.instance.players;

            // disable excess player trade buttons
            for (int i = players.Count; i < tradePlayersButtons.Count; i++)
            {
                tradePlayersButtons[i].gameObject.SetActive(false);
            }

            // setup player trade buttons
            for (int i = 0; i < players.Count; i++)
            {
                if(players[i] == currentPlayer)
                {
                    tradePlayersButtons[i].interactable = false;
                    tradePlayersButtons[i].GetComponentInChildren<TMP_Text>().text = "YOU";
                    continue;
                }
                tradePlayersButtons[i].GetComponentInChildren<TMP_Text>().text = players[i].name;
            }

            // set available player trades
            foreach(var resource in currentPlayer.resources)
            {
                if (resource.Key == ResourceCardType.None || resource.Key == ResourceCardType.Desert)
                    continue;

                if(resource.Value != 0)
                {
                    offerAddButtons[(int)resource.Key].interactable = true;
                }
            }
        }

        // tradee chosen
        public void TradeWith(int playerIndex)
        {
            tradee = players[playerIndex];
            Instantiate(tradeConfirmDialog);
        }

        public void CancelTrade()
        {
            Destroy(gameObject);
        }

        // update trade resource, check available tradees and disable buttons if player can't offer more of this type of resource
        public void UpdateTradeResource(ResourceButtons buttonScript, ResourceCardType resourceType, int amount, int receive)
        {
            if (receive != 0)
            {
                receiving[resourceType] += amount;
                receiveResourceText[(int)resourceType].text = receiving[resourceType].ToString();
                if(receiving[resourceType] == 0)
                {
                    buttonScript.buttonRemove.interactable = false;
                } else
                {
                    buttonScript.buttonRemove.interactable = true;
                }

                // check if trade available for other players
                for(int i = 0; i < players.Count; i++)
                {
                    Player player = players[i];

                    if(player == currentPlayer)
                    {
                        continue;
                    }

                    bool canTrade = true;
                    foreach(var resource in receiving)
                    {
                        if(resource.Value != 0 && (!player.resources.ContainsKey(resource.Key) || player.resources[resource.Key] < receiving[resource.Key]))
                        {
                            canTrade = false;
                            break;
                        }
                    }

                    tradePlayersButtons[i].interactable = canTrade;

                }
            }
            else
            {
                offering[resourceType] += amount;
                offerResourceText[(int)resourceType].text = offering[resourceType].ToString();
                if (offering[resourceType] == 0)
                {
                    buttonScript.buttonRemove.interactable = false;
                }
                else
                {
                    buttonScript.buttonRemove.interactable = true;
                }

                if(currentPlayer.resources.ContainsKey(resourceType))
                {
                    if(currentPlayer.resources[resourceType] == offering[resourceType])
                    {
                        buttonScript.buttonAdd.interactable = false;
                    } else
                    {
                        buttonScript.buttonAdd.interactable = true;
                    }
                }
            }
        }
    }
}
