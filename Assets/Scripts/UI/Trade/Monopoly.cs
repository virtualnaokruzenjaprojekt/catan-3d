using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using StateMachine;

public class Monopoly : MonoBehaviour
{
    public int resource = -1;
    public Image[] resourceButton; // resouce button background
    public Canvas overlayCanvas;
    public Button confirmButton;
    public Color colorUnselected;
    public Color colorSelected;

    void Start()
    {
        overlayCanvas.worldCamera = Camera.main;
    }

    // execute monopoly if resource is selected
    public void ConfirmMonopoly()
    {
        if (resource != -1)
        {
            // CALL: monopoly function
            UIManager.instance.PlayMonopoly((ResourceCardType)resource);
        }

        Destroy(gameObject);
    }

    // select resource for monopoly
    public void SelectResource(int index)
    {
        if (resource != -1)
        {
            resourceButton[resource].color = colorUnselected;
        }

        resource = index;
        resourceButton[index].color = colorSelected;

        if (resource != -1)
        {
            confirmButton.interactable = true;
        }
    }

    public void CancelMonopoly()
    {
        Destroy(gameObject);
    }
}
