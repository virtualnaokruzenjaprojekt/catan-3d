using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Trade;

public class TradeDialog : MonoBehaviour
{
    public Canvas overlayCanvas;
    public TMP_Text headerText;

    private void Start()
    {
        overlayCanvas.worldCamera = Camera.main;
        headerText.text = "Confirm trade with player " + TradeManager.instance.tradee.name;
    }

    public void RefuseTrade()
    {
        TradeManager.instance.tradee = null; // set tradee back to null
        Destroy(gameObject);
    }

    // player accepts trade
    public void AcceptTrade()
    {
        // resource list
        List<int> offerList = new(5) { 0, 0, 0, 0, 0 };
        List<int> receiveList = new(5) { 0, 0, 0, 0, 0 };

        // translate offering resources
        foreach(var resource in TradeManager.instance.offering)
        {
            offerList[(int)resource.Key] = resource.Value;
        }        
        // translate receiving resources
        foreach (var resource in TradeManager.instance.receiving)
        {
            receiveList[(int)resource.Key] = resource.Value;
        }

        UIManager.instance.ConfirmTradeButton(TradeManager.instance.tradee, offerList, receiveList);
        Destroy(TradeManager.instance.gameObject);
        Destroy(gameObject);
    }
}
