using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TradeButton : MonoBehaviour
{
    public ResourceCardType cardType;
    public int amount; // amount to be added to trade
    public int receive; // flag if resource is being offered or received
    public ResourceButtons resourceButtons; // call parent script

    // update resources in parent script
    public void AddToTrade()
    {
        resourceButtons.UpdateResource(cardType, amount, receive);
    }
}
