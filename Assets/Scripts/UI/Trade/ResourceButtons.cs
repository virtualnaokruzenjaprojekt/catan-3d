using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Trade;

public class ResourceButtons : MonoBehaviour
{

    public Button buttonAdd;
    public Button buttonRemove;

    // call existing trade manager and update trade resource
    public void UpdateResource(ResourceCardType cardType, int amount, int receive)
    {
        TradeManager.instance.UpdateTradeResource(this, cardType, amount, receive);
    }
}
