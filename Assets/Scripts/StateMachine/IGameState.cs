using System;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace StateMachine
{
    public interface IGameState
    {

        public void startState() {}

        public void initBoard(){}
        public void initialBuilding(){}
        public void playerTurn(){}

        public void throwDice() {}
        
        public void ChooseAction(){}

        public void ThrowFirstDice() { }
        
        public void chooseLocation() { }

    }
}