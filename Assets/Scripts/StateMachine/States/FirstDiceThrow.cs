﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StateMachine.States
{
    class FirstDiceThrow : IGameState
    {
        private GameManager game;
        public FirstDiceThrow(GameManager game)
        {
            this.game = game;
            // this.ThrowFirstDice();
        }
        public void startState()
        {
            this.ThrowFirstDice();
        }
        public void ThrowFirstDice()
        {
            
            foreach(Player player in game.players){
                game.dice.throwDice();
                player.SetLastDiceThrowNumber(game.dice.getDiceNumber());
            }
            game.dice.ResetDice();
            game.ChooseFirstActivePlayer();
        }
    }
}
