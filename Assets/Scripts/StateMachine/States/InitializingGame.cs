using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DefaultNamespace;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

namespace StateMachine.States
{
    public class InitializingGame : IGameState
    {
        GameManager game;
        public InitializingGame(GameManager game)
        {
            this.game = game;
        }
        
        public void startState()
        {
            this.initBoard();

        }
        public void initBoard()
        {
            EnableCameraAndEventSystem();
            game.DisableEnableUIActions();
            Debug.Log("Initializing Game");
            game.board.InitalizeBoard();
            Debug.Log("Game Initialized");
            CreatePlayers();
            game.ChangeState(new FirstDiceThrow(game));
        }

        private void EnableCameraAndEventSystem()
        {
            Debug.Log("acitvae scene " + SceneManager.GetActiveScene().name);
            GameObject boardCamera = GameObject.FindGameObjectWithTag("BoardCamera");
            GameObject boardEventSystem = GameObject.FindWithTag("BoardEvent");
            
            // GameObject[] allObjects = UnityEngine.Object.FindObjectsOfType<GameObject>();
            //
            // // Print information about each GameObject
            // foreach (GameObject obj in allObjects)
            // {
            //     if (obj.name == "Camera")
            //     {
            //         // Print the name of the GameObject
            //         Debug.Log("Object Name: " + obj.name);
            //
            //         // Print the tag of the GameObject
            //         Debug.Log("Tag: " + obj.tag);
            //
            //         // Print the layer of the GameObject
            //         Debug.Log("Layer: " + LayerMask.LayerToName(obj.layer));
            //
            //         // You can print additional information based on your needs
            //
            //         // Add a separator for better readability
            //         Debug.Log("-------------------------------------");
            //     }
            //
            // }

            // Check if the EventSystem with the specified tag is found

            if ( boardEventSystem != null && boardCamera != null)
            { 
                EventSystem mainMenuEventSystemComponent = boardEventSystem.GetComponent<EventSystem>();
                AudioListener audioListener = boardCamera.GetComponent<AudioListener>();
                Camera camera = boardCamera.GetComponent<Camera>();
                FlyCamera flyCamera = boardCamera.GetComponent<FlyCamera>();
                if (boardEventSystem != null && audioListener != null && camera != null && flyCamera != null)
                {
                    mainMenuEventSystemComponent.enabled = true;
                    audioListener.enabled = true;
                    camera.enabled = true;
                    flyCamera.enabled = true;
                    Debug.Log("audioListener and eventSystem component enabled on the GameObject with tag 'BoardCamera'.");
                }
                else
                {
                    Debug.LogWarning("audioListener component not found on the GameObject with tag 'BoardCamera'.");
                }
            
            }
            else

            {
                Debug.LogWarning("GameObject with tag 'BoardCamera' not found.");
            }
        }

        private void CreatePlayers()
        {
            List<string> players = GameManager.playerNames;
            int PlayerNumber = 0;
            Debug.Log("Player names count: " + GameManager.playerNames.Count);
            foreach(string playerName in players)
            {
                Player p = GameManager.instance.gameObject.AddComponent<Player>();
                p.name = playerName;
                p.Color = (PlayerColor)PlayerNumber++;
                foreach (ResourceCardType resource in Enum.GetValues(typeof(ResourceCardType)))
                {
                    p.resources.Add(resource, 0);
                }
                foreach (DevelopmentCardType developmentCard in Enum.GetValues(typeof(DevelopmentCardType)))
                {
                    p.availableDevelopmentCards.Add(developmentCard, 0);
                    p.turnDelayedDevelopmentCards.Add(developmentCard, 0);
                    p.usedDevelopmentCards.Add(developmentCard, 0);
                }
                game.players.Add(p);
            }


        }

    }
}