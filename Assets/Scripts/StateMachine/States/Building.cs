﻿using System.Diagnostics;
using System.Threading.Tasks;
using DefaultNamespace;
using Debug = UnityEngine.Debug;

namespace StateMachine.States
{
    public class Building : IGameState
    {
        GameManager gameManager;
        bool freeBuilding = false;

        public Building(GameManager game, bool freeBuilding = false)
        {
            this.freeBuilding = freeBuilding;
            gameManager = game;
        }

        public void startState()
        {
            building();
        }

        public void building()
        {
            BuildingResult buildingResult = BuildingResult.LocationNotSelected;
            if (gameManager.neededBuildingType == BuildingTypeName.Settlement)
            {
                buildingResult = gameManager.BuildSettlement(freeBuilding);
            }
            else if (gameManager.neededBuildingType == BuildingTypeName.Road)
            {
                buildingResult = gameManager.BuildRoad(freeBuilding);
            }
            else if (gameManager.neededBuildingType == BuildingTypeName.City)
            {
                buildingResult = gameManager.BuildCity(freeBuilding);
            }
            
            if (buildingResult is BuildingResult.Success)
            {
                if (gameManager.neededBuildingNumber > 0)
                {
                    gameManager.ChangeState(new Building(gameManager, freeBuilding));
                }
                else
                {
                    gameManager.ChangeState(new PlayerTurn(gameManager));
                }
            }
            
        }
    }
}