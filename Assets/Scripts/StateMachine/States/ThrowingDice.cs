using UnityEngine;

namespace StateMachine.States
{
    public class ThrowingDice : IGameState
    {
        private GameManager game;
        public ThrowingDice(GameManager game)
        {
            this.game = game;
        }
        
        public void startState()
        {
            throwDice();
        }
        public void throwDice()
        {
            game.dice.throwDice();
            game.ChangeState(new PlayerTurn(game));
        }

}
}