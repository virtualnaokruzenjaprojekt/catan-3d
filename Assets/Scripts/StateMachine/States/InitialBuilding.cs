using System.Diagnostics;
using System.Threading.Tasks;
using DefaultNamespace;
using Debug = UnityEngine.Debug;

namespace StateMachine.States
{
    public class InitialBuilding : IGameState
    {
        GameManager gameManager;
        public InitialBuilding(GameManager game)
        {
            gameManager = game;
            // initialBuilding();
        }
        
        public void startState()
        {
            initialBuilding();
        }
        public void initialBuilding()
        {
            int initialBuildingStep = gameManager.activePlayer.initialBuildingStep;
            BuildingResult buildingResult = BuildingResult.LocationNotSelected;
            if (initialBuildingStep is 0 or 3)
            {
                gameManager.neededBuildingType = BuildingTypeName.Settlement;
                buildingResult = gameManager.BuildSettlement(true);
                if (buildingResult == BuildingResult.Success && initialBuildingStep == 3)
                {
                    gameManager.allLastActiveCrossroads.AddRange(gameManager.lastActiveCrossroads);
                }
              
            }
            else if (initialBuildingStep is 1 or 2 or 4 or 5)
            {
                gameManager.neededBuildingType = BuildingTypeName.Road;
                buildingResult = gameManager.BuildRoad(true);
            }
            else
            {
                Debug.Log("Initial Building Complete");
                gameManager.firstTurn = false;
                gameManager.GiveResourcesToPlayers();
                gameManager.ChangeState(new PlayerTurn(gameManager));
            }

            if (buildingResult == BuildingResult.Success)
            {
                // Debug.Log("Initial Building Step Complete");
                gameManager.neededBuildingType = BuildingTypeName.None;
                gameManager.activePlayer.initialBuildingStep++;

                if (initialBuildingStep == 2)
                {
                    // If last player dont change player if on 2 building step
                    if (gameManager.players[gameManager.GetIndexForNextPlayer(true)].position != 0 )
                    {
                        gameManager.SetNextPlayer(true);

                    }
                    
                }
                else if (initialBuildingStep == 5 &&  gameManager.activePlayer.position != 0 )
                {
                    gameManager.SetNextPlayer();
                }

                initialBuilding();
            }
        }
    }
}