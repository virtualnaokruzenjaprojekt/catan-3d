﻿using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

namespace StateMachine.States
{
    public class MainMenu : IGameState
    {
        GameManager gameManager;
        public MainMenu(GameManager game)
        {
            gameManager = game;
        }
        
        public void startState()
        {
            ShowingMainMenu();
        }
        public void ShowingMainMenu()
        {
            GameObject boardCamera = GameObject.FindGameObjectWithTag("BoardCamera");
            GameObject eventSystem = GameObject.FindGameObjectWithTag("BoardEvent");

            // Check if the EventSystem with the specified tag is found
            if (boardCamera != null && eventSystem != null)
            {
                Camera camera = boardCamera.GetComponent<Camera>();
                AudioListener audioListener = boardCamera.GetComponent<AudioListener>();
                EventSystem eventSystemComponent = eventSystem.GetComponent<EventSystem>();
                FlyCamera flyCamera = boardCamera.GetComponent<FlyCamera>();

                if (camera != null && eventSystemComponent != null && audioListener != null && flyCamera != null)
                {
                    camera.enabled = false;
                    eventSystemComponent.enabled = false;
                    audioListener.enabled = false;
                    flyCamera.enabled = false;
                    Debug.Log("audioListener and eventSystem component disabled on the GameObject with tag 'BoardCamera'.");
                }
                else
                {
                    Debug.LogWarning("audioListener component not found on the GameObject with tag 'BoardCamera'.");
                }
            }
            else
            {
                Debug.LogWarning("GameObject with tag 'BoardCamera' not found.");
            }
            
            SceneManager.LoadScene("Scenes/Menu", LoadSceneMode.Additive);
        }
    }
}