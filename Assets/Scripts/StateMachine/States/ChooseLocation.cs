﻿using System.Threading.Tasks;
using UnityEngine;

namespace StateMachine.States
{
    public class ChooseLocation : IGameState
    {
        GameManager gameManager;
        public bool freeBuilding = false;
        public ChooseLocation(GameManager game, bool freeBuilding = false)
        {
            gameManager = game;
            this.freeBuilding = freeBuilding;
        }
        public void chooseLocation()
        {
            if (gameManager.firstTurn)
            {
                gameManager.ChangeState(new InitialBuilding(gameManager));
            }
            else
            {
                gameManager.ChangeState(new Building(gameManager, freeBuilding));
            }
        }
    }
}