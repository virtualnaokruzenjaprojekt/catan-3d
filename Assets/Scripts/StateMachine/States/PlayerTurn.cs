using System;
using System.Collections.Generic;
using UnityEngine;

namespace StateMachine.States
{
    public class PlayerTurn : IGameState
    {
        private GameManager game;
        public PlayerTurn(GameManager game)
        {
            this.game = game;
        }
        
        public void startState()
        {
            playerTurn();
        }
        public void playerTurn()
        {
            if (!game.playerTurnInProgress)
            {
                game.StartPlayerTurn();
            }
        }
    }
}
