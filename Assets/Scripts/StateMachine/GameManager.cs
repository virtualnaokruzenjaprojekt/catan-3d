using System;
using StateMachine.States;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;
using Button = UnityEngine.UI.Button;
using Random = UnityEngine.Random;

namespace StateMachine
{
    public enum BuildingResult
    {
        LocationNotSelected,
        NotEnoughResources,
        Success,
    }
    public class GameManager : MonoBehaviour
    {
        private IGameState _state;
        public Dictionary<DevelopmentCardType, int> developmentDeck = new();
        public Dice dice;
        public Board board;

        public List<Player> players = new();
        public static List<string> playerNames;
        public Player activePlayer;
        public bool firstTurn = true;
        public bool chooseTheifLocation = false;
        public bool devCardInUse = false;

        // TO DO: CHANGE THIS TO THE CORRECT CLASS 
        public Border lastClickedRoad;
        public Crossroad lastClickedCrossroad;
        public Hexagon lastClickedHexagon;
        public List<Crossroad> lastActiveCrossroads = new List<Crossroad>();
        public List<Crossroad> allLastActiveCrossroads = new List<Crossroad>();
        public BuildingTypeName neededBuildingType;
        public int neededBuildingNumber = 0;
        public bool playerTurnInProgress = false;
        
        public int largestArmy = 2;
        public Player largestArmyOwner = null;
        public int longestRoad = 4;
        public Player longestRoadOwner = null;
        public List<int> activeFieldNumbers = new();
        public FlyCamera flyCamera;

        #region SINGLETON
        public static GameManager instance = null;

        void Start()
        {

            if (instance is null) { 
                instance = this;
                developmentDeck.Add(DevelopmentCardType.Knight, 5);
                developmentDeck.Add(DevelopmentCardType.Monopoly, 2);
                developmentDeck.Add(DevelopmentCardType.RoadBuilding, 2);
                developmentDeck.Add(DevelopmentCardType.YearOfPlenty, 2);
                developmentDeck.Add(DevelopmentCardType.VictoryPoint, 2);
                flyCamera = GameObject.FindGameObjectWithTag("BoardCamera").GetComponent<FlyCamera>();
                Debug.Log("Starting Game Manager");
                
            }
            else
            {
                Destroy(gameObject);
                Debug.LogError("ANOTHER GAME MANAGER INITATED, THIS SOULD NOT HAPPEN");

            }
            Debug.Log("Initializing Game Called from GameManager Start");
            this.ChangeState(new MainMenu(this));


        }
        #endregion
        
        public void ChangeState(IGameState state)
        {
            _state = state;
            _state.startState();
            if (_state is PlayerTurn && !chooseTheifLocation) {
                GameManager.instance.DisableEnableUIActions(true);
                flyCamera.enabled = false;
            } else if(_state is ChooseLocation)
            {
                flyCamera.enabled = true;
            }
            Debug.LogWarning("State changed to " + _state.GetType().Name);
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                flyCamera.enabled = !flyCamera.enabled;
            }
        }
        
        public void setLastClickedRoad(Border border)
        {
            bool canBuildRoad = false;
            Crossroad[] crossroads = border.crossroads;
            foreach (Crossroad crossroad in crossroads)
            {
                if (crossroad.owner == activePlayer)
                {
                    canBuildRoad = true;
                }
                foreach (Border crossroadBorder in crossroad.borders)
                {
                    if (crossroadBorder.owner == activePlayer)
                    {
                        canBuildRoad = true;
                    }
                }
            }

            if (neededBuildingType == BuildingTypeName.Road && canBuildRoad)
            {
                lastClickedRoad = border;
            }
        }
        
        public void setLastClickedCrossroad(Crossroad crossroad)
        {
            Debug.LogWarning("last clicked crossroad set and BuildingTypeName " + neededBuildingType);
            if (neededBuildingType == BuildingTypeName.Settlement)
            {
                if ((firstTurn || CheckIfConnectedWithRoad(crossroad)) && checkIfNotNeighbouring(crossroad))
                {
                    lastClickedCrossroad = crossroad;
                }
            }
            else if (neededBuildingType == BuildingTypeName.City)
            {
                if (crossroad.owner == activePlayer && crossroad.buildingType == BuildingTypeName.Settlement)
                {
                    lastClickedCrossroad = crossroad;
                }
            }
        }

        private bool checkIfNotNeighbouring(Crossroad crossroad)
        {
            foreach(Border boarder in crossroad.borders)
            {
                foreach (Crossroad crossroad1 in boarder.crossroads)
                {
                    if (crossroad1.owner != null)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        private bool CheckIfConnectedWithRoad(Crossroad crossroad)
        {
            foreach (Border border in crossroad.borders)
            {
                if (border.owner == activePlayer)
                {
                    return true;
                }
            }
            return false;
        }

        public void ChooseFirstActivePlayer()
        {
            Debug.Log("Choosing first active player");
            activePlayer = players.Find(player => player.GetLastDiceThrowNumber() == players.Max(player => player.GetLastDiceThrowNumber()));
            int activePlayerIndex =  players.IndexOf(activePlayer);
            for (int i = 0; i < players.Count; i++)
            {
                players[(i + activePlayerIndex) % players.Count].position = i;
            }
            this.ChangeState(new InitialBuilding(this));
        }
        
        public void SetNextPlayer(bool firstTurn = false)
        {
            int index = GetIndexForNextPlayer(firstTurn);
            activePlayer = players[index];;
            
        }

        public void StartPlayerTurn()
        {
            SetNextPlayer();
            DisableEnableUIActions(true);

            //HANDLE EVERYTHING
            Utils.TranslateDevelopmentCards(activePlayer.turnDelayedDevelopmentCards, activePlayer.availableDevelopmentCards);
            playerTurnInProgress = true;
            int diceNumber = dice.getDiceNumber();
            if (diceNumber == Dice.DICE_RESET)
            {
                ChangeState(new ThrowingDice(this));
            }
            diceNumber = dice.getDiceNumber();

            if (diceNumber != Dice.DICE_RESET && diceNumber != 7)
            {
                UIManager.instance.SetDiceNumber(diceNumber);
                GiveResourcesToPlayers(diceNumber);
            }
            else if (diceNumber == 7)
            {
                Debug.Log("Handle move robber");
                UIManager.instance.SetDiceNumber(diceNumber);
                chooseTheifLocation = true;
                DisableEnableUIActions();
                flyCamera.enabled = true;
            }
        }

        public int GetIndexForNextPlayer(bool firstTurn = false)
        {
            int index = players.IndexOf(activePlayer);
            if (firstTurn) index = (index + (players.Count - 1)) % players.Count;
            else index = (index + 1) % players.Count;
            return index;
        }
        
        public void ChooseLocation()
        {
            _state.chooseLocation();
        }


        public void DisableEnableUIActions(bool enable = false)
        {
            GameObject[] actionButtons = GameObject.FindGameObjectsWithTag("ActionButton");
            foreach (GameObject button in actionButtons)
            {
                button.GetComponent<Button>().interactable = enable;
            }
        }
        
        public void EndPlayerTurn()
        {
            CheckNewLongestRoad();
            playerTurnInProgress = false;
            dice.ResetDice();
            CheckIfPlayerWon();
            ChangeState(new PlayerTurn(this));
        }
        
        public void CheckIfPlayerWon()
        {
            if (activePlayer.points >= 10)
            {
                Winner winnerScript = GameObject.FindGameObjectWithTag("Winner").GetComponent<Winner>();
                winnerScript.overlayCanvas.enabled = true;
                winnerScript.SetWinner(activePlayer.name);
            }
        }
        public DevelopmentCardType ChooseDevelopmentCardFromDeck()
        {
            int totalCards = 0;

            foreach (var count in developmentDeck.Values)
            {
                totalCards += count;
            }

            int randomIndex = Random.Range(0, totalCards);

            foreach (var pair in developmentDeck)
            {
                randomIndex -= pair.Value;

                if (randomIndex <= 0)
                {
                    developmentDeck[pair.Key]--;

                    if (developmentDeck[pair.Key] <= 0)
                    {
                        developmentDeck.Remove(pair.Key);
                    }

                    return pair.Key;
                }
            }

            Debug.LogError("Code SHOULD NOT REACH THIS");
            return DevelopmentCardType.Knight;
        }

        public void GiveResourcesToPlayers()
        {
            foreach (Crossroad crossroad in allLastActiveCrossroads)
            {
                foreach (Hexagon hex in crossroad.hexagons)
                {
                    Tuple<ResourceCardType, int> resources = crossroad.GrabResource(hex.fieldNumber);
                    crossroad.owner.resources[resources.Item1] += resources.Item2;
                    Debug.Log("Giving " + resources.Item2 + " " + resources.Item1 + " to " + crossroad.owner.name);
                }

            }
        }

        public void GiveResourcesToPlayers(int diceNumber)
        {
            List<Hexagon> hexes = board.GetHexagonFromDiceNumber(diceNumber);
            foreach (Hexagon hex in hexes)
            {
                foreach (GameObject crossroadObject in hex.crossroads)
                {
                    Crossroad crossroad = crossroadObject.GetComponent<Crossroad>();
                    if (crossroad.owner != null)
                    {
                        Tuple<ResourceCardType, int> resources = crossroad.GrabResource(hex.fieldNumber);
                        crossroad.owner.resources[resources.Item1] += resources.Item2;
                        Debug.Log("Giving " + resources.Item2 + " " + resources.Item1 + " to " + crossroad.owner.name);

                    }
                }
            }
        }
        
        public void HandleBuyDevelopmentCard()
        {
            if(Utils.CanBuyDevelopmentCard(developmentDeck))
            {
                while(true)
                {
                    int random = Random.Range(0, 5);

                    if(activePlayer.turnDelayedDevelopmentCards.ContainsKey((DevelopmentCardType)random)) {
                        activePlayer.turnDelayedDevelopmentCards[(DevelopmentCardType)random]++;
                    } else
                    {
                        activePlayer.turnDelayedDevelopmentCards.Add((DevelopmentCardType)random, 1);
                    }

                    activePlayer.resources[ResourceCardType.Wool]--;
                    activePlayer.resources[ResourceCardType.Ore]--;
                    activePlayer.resources[ResourceCardType.Grain]--;

                    return;
                }
            }
        }

        public bool DeckIsEmpty()
        {
            int n = 0;

            foreach (var pair in developmentDeck)
            {
                n += pair.Value;
            }

            if (n > 0) return true;
            else return false;
        }

        public bool UseCard(DevelopmentCardType developmentCard)
        {

            Player player = activePlayer;
            if (!player.availableDevelopmentCards.ContainsKey(developmentCard)) return false;

            if (player.availableDevelopmentCards[developmentCard] > 0) player.availableDevelopmentCards[developmentCard]--;
            else return false;

            if (player.usedDevelopmentCards.ContainsKey(developmentCard)) player.usedDevelopmentCards[developmentCard]++;
            else player.usedDevelopmentCards.Add(developmentCard, 1);

            switch (developmentCard)
            {
                case DevelopmentCardType.Knight:
                    activePlayer.AddKnight();
                    flyCamera.enabled = true;
                    chooseTheifLocation = true;
                    devCardInUse = true;
                    DisableEnableUIActions();
                    flyCamera.enabled = true;
                    break;
                case DevelopmentCardType.Monopoly:
                    UIManager.instance.OpenMonopolyDialog();
                    break;
                case DevelopmentCardType.RoadBuilding:
                    GameManager.instance.DisableEnableUIActions();
                    flyCamera.enabled = true;
                    GameManager.instance.neededBuildingNumber = 2;
                    GameManager.instance.neededBuildingType = BuildingTypeName.Road;
                    GameManager.instance.ChangeState(new Building(GameManager.instance, true));
                    break;
                case DevelopmentCardType.VictoryPoint:
                    player.AddPoint();
                    break;
                case DevelopmentCardType.YearOfPlenty:
                    UIManager.instance.OpenYOPDialog();
                    break;
            }

            return true;
        }

        public void HandleMoveRobber(Hexagon hex = null)
        {
            DisableEnableUIActions(true);

            if (!devCardInUse)
            {
                //For players who have more than 8 cards remove random half
                foreach (Player player in players)
                {
                    int resources = 0;

                    foreach (var resource in player.resources)
                    {
                        if (resource.Key == ResourceCardType.Desert || resource.Key == ResourceCardType.None)
                            continue;

                        resources += resource.Value;
                    }

                    if (resources >= 8)
                    {
                        int original = resources;
                        while (resources > original / 2)
                        {
                            while (true)
                            {
                                int pickIndex = Random.Range(0, 5);
                                if (player.resources.ContainsKey((ResourceCardType)pickIndex))
                                {
                                    if (player.resources[(ResourceCardType)pickIndex] != 0)
                                    {
                                        player.resources[(ResourceCardType)pickIndex]--;
                                        resources--;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                List<Player> adjecentPlayers = new(); //TODO: GET THIS FROM HEX OBJECTS
                if (hex != null)
                {
                    foreach (GameObject crossRoadObject in hex.crossroads)
                    {
                        Crossroad crossroad = crossRoadObject.GetComponent<Crossroad>();
                        if (crossroad.owner != null && !adjecentPlayers.Contains(crossroad.owner) && crossroad.owner != activePlayer)
                        {
                            adjecentPlayers.Add(crossroad.owner);
                        }
                    }
                }

                Debug.LogWarning("adjecent players count: " + adjecentPlayers.Count);
                if (adjecentPlayers.Count == 0)
                {
                    devCardInUse = false;
                    return;
                }
                //Take a random card from random player
                foreach (var player in adjecentPlayers)
                {
                    bool keep = false;
                    foreach (var resource in player.resources)
                    {
                        if (resource.Key == ResourceCardType.Desert || resource.Key == ResourceCardType.None)
                            continue;

                        if (resource.Value != 0)
                        {
                            keep = true;
                            break;
                        }
                    }

                    if (!keep) adjecentPlayers.Remove(player);
                }

                if (adjecentPlayers.Count == 0) return;

                Player takeCardFrom = adjecentPlayers[Random.Range(0, adjecentPlayers.Count)];
                while (true)
                {
                    int chooseCard = Random.Range(0, 5);
                    if (takeCardFrom.resources.ContainsKey((ResourceCardType)chooseCard))
                    {
                        if (takeCardFrom.resources[(ResourceCardType)chooseCard] != 0)
                        {
                            takeCardFrom.resources[(ResourceCardType)chooseCard]--;
                            activePlayer.resources[(ResourceCardType)chooseCard]++;
                            break;
                        }
                    }
                }
            }

            devCardInUse = false;
        }

        public void NewLargestArmy(Player player, int armySize)
        {
            largestArmy = armySize;
            if (largestArmyOwner != null)
            {
                largestArmyOwner.RemovePoints(2);
            }

            largestArmyOwner = player;
            player.AddPoint(2);
        }

        public void CheckNewLongestRoad() {
            int roadLength = longestRoad;
            foreach (Border border in activePlayer.roadList)
            {
                HashSet<Border> visited = new HashSet<Border>();
                int newRoadLength = GetLongestRoadLen(border,  visited);
                if (newRoadLength > roadLength)
                {
                    roadLength = newRoadLength;
                }
            }
            if (roadLength <= longestRoad) return;
            Debug.Log("New longest road is " + roadLength);
            longestRoad = roadLength;
            if (longestRoadOwner != null) longestRoadOwner.RemovePoints(2);
            longestRoadOwner = activePlayer;
            activePlayer.AddPoint(2);
        }

        public int GetLongestRoadLen(Border border, HashSet<Border> visited)
        {
            if (visited.Contains(border))
            {
                return 0;
            }

            visited.Add(border);

            int currentLength = 1;
            List<Border> connectedBorders = new List<Border>();
            connectedBorders.AddRange(border.crossroads[0].borders);   
            connectedBorders.AddRange(border.crossroads[1].borders);
            foreach (var neighbor in connectedBorders)
            {
                if (neighbor.owner == border.owner)
                {
                    currentLength += GetLongestRoadLen(neighbor, visited);
                }
            }

            return currentLength;
        }
        
        

        #region Building and Development
        public BuildingResult BuildRoad(bool firstTurn = false)
        {
            if (lastClickedRoad == null)
            {
                ChangeState(new ChooseLocation(this, firstTurn));
                return BuildingResult.LocationNotSelected;
            }
             bool result = activePlayer.TryBuild(new Road(), lastClickedRoad, firstTurn: firstTurn);
             if (result)
             {
                 return BuildingResult.Success;
             }

             return BuildingResult.NotEnoughResources;
        }

        public BuildingResult BuildSettlement(bool firstTurn = false)
        {
            if (lastClickedCrossroad == null)
            {
                ChangeState(new ChooseLocation(this, firstTurn));
                return BuildingResult.LocationNotSelected;
            }
            lastActiveCrossroads = new();
            lastActiveCrossroads.Add(lastClickedCrossroad);

            bool result = activePlayer.TryBuild(new Settlement(), lastClickedCrossroad, firstTurn: firstTurn);
            if (result)
            {
                return BuildingResult.Success;
            }

            return BuildingResult.NotEnoughResources;
        
        }

        public BuildingResult BuildCity(bool firstTurn = false)
        {
            if (lastClickedCrossroad == null)
            {
                ChangeState(new ChooseLocation(this, firstTurn));
                return BuildingResult.LocationNotSelected;
            }
            bool result = activePlayer.TryBuild(new City(), lastClickedCrossroad, firstTurn: firstTurn);
            if (result)
            {
                return BuildingResult.Success;
            }

            return BuildingResult.NotEnoughResources;
        }

        public void HandleMonopolyCard(ResourceCardType resource)
        {
            if (activePlayer.availableDevelopmentCards.ContainsKey(DevelopmentCardType.Monopoly)) {
                if (activePlayer.availableDevelopmentCards[DevelopmentCardType.Monopoly] != 0)
                {
                    activePlayer.availableDevelopmentCards[DevelopmentCardType.Monopoly]--;
                }
                else {
                    return;
                }
            }
            else
            {
                return;
            }
                

            int count = 0;

            foreach(Player player in players)
            {
                if (player == activePlayer) continue;

                count += player.resources[resource];
                player.resources[resource] = 0;
            }

            activePlayer.resources[resource] += count;
        }

        public void HandleYOPCard(ResourceCardType resource1, ResourceCardType resource2)
        {
            if (activePlayer.availableDevelopmentCards.ContainsKey(DevelopmentCardType.YearOfPlenty))
            {
                if (activePlayer.availableDevelopmentCards[DevelopmentCardType.YearOfPlenty] != 0)
                {
                    activePlayer.availableDevelopmentCards[DevelopmentCardType.YearOfPlenty]--;
                }
                else
                {
                    return;
                }
            }
            else
            {
                return;
            }
            activePlayer.resources[resource1]++;
            activePlayer.resources[resource2]++;
        }

        #endregion

        #region Trading

        public void TradeResourcesWithShop(ResourceCardType giving, ResourceCardType receiving)
        {
            activePlayer.resources[giving] -= 4;
            activePlayer.resources[receiving] += 1;
        }

        public void ExecuteTrade(Player tradee, List<int> give, List<int> get)
        {
            activePlayer.TradeInResources(give);
            activePlayer.GetFromTrade(get);

            tradee.TradeInResources(get);
            tradee.GetFromTrade(give);
        }

        #endregion

        public void setLastClickedHexagon(Hexagon hexagon)
        {
            if (chooseTheifLocation)
            {
                foreach (Transform hexagonTransform in board.hexagons)
                {
                    Hexagon hex = hexagonTransform.GetComponent<Hexagon>();
                    hex.SetRobber(false);
                }
                hexagon.SetRobber(true);
                chooseTheifLocation = false;
                flyCamera.enabled = false;
                HandleMoveRobber(hexagon);
            }
        }
    }
}