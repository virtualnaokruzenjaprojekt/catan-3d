using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using StateMachine;
using StateMachine.States;
using TMPro;

public class UIManager : MonoBehaviour
{
    public GameObject tradeDialog;
    public GameObject shopDialog;
    public GameObject yopDialog;
    public GameObject monopolyDialog;
    public GameObject confirmTradeDialog;
    public TMP_Text currentPlayerText;
    public TMP_Text playerPoints;
    public TMP_Text currentDiceNumber;
    public List<TMP_Text> resources;
    public List<TMP_Text> developments;
    public FlyCamera flyCamera;
    public GameObject boardCamera;
    #region SINGLETON
    public static UIManager instance = null;

    void Start()
    {
        if (instance is null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
            Debug.LogError("ANOTHER GAME MANAGER INITATED, THIS SOULD NOT HAPPEN");

        }

        boardCamera = GameObject.FindGameObjectWithTag("BoardCamera");
        flyCamera = boardCamera.GetComponent<FlyCamera>();
    }
    #endregion

    public void SetDiceNumber(int number)
    {
        currentDiceNumber.text = "Dice: " + number.ToString();
    }

    private void Update()
    {
        if (GameManager.instance is null || GameManager.instance.activePlayer is null)
            return;

        foreach (var resource in GameManager.instance.activePlayer.resources)
        {
            if (resource.Key == ResourceCardType.None || resource.Key  == ResourceCardType.Desert)
                continue;
            resources[(int)resource.Key].text = resource.Value.ToString();
        }

        foreach (var developmentCard in GameManager.instance.activePlayer.availableDevelopmentCards)
        {
            developments[(int)developmentCard.Key].text = developmentCard.Value.ToString();
        }

        currentPlayerText.text = GameManager.instance.activePlayer.name;
        playerPoints.text = GameManager.instance.activePlayer.points.ToString();
        currentPlayerText.color = ColorMapping.GetUnityColor(GameManager.instance.activePlayer.Color);
    }
    
    public void OpenShopDialog()
    {
        Instantiate(shopDialog);
    }

    public void OpenMonopolyDialog()
    {
        if (GameManager.instance.activePlayer.availableDevelopmentCards[DevelopmentCardType.Monopoly] == 0) return;
        Instantiate(monopolyDialog);
    }

    public void OpenYOPDialog()
    {
        if (GameManager.instance.activePlayer.availableDevelopmentCards[DevelopmentCardType.YearOfPlenty] == 0) return;
        Instantiate(yopDialog);
    }
    
    public void OpenTradeDialog()
    {
        Instantiate(tradeDialog);
    }

    public void AskTradeeToConfirm()
    {
        Instantiate(confirmTradeDialog);
    }

    public void ConfirmTradeButton(Player tradee, List<int> offer, List<int> receive)
    {
        GameManager.instance.ExecuteTrade(tradee, offer, receive);
    }

    public void PlayMonopoly(ResourceCardType resource)
    {
        GameManager.instance.HandleMonopolyCard(resource);
    }

    public void PlayYOP(ResourceCardType resource1, ResourceCardType resource2)
    {
        GameManager.instance.HandleYOPCard(resource1, resource2);
    }

    public void PlayVictoryPoint()
    {
        GameManager.instance.UseCard(DevelopmentCardType.VictoryPoint);
    }

    public void PlayKnight()
    {
        GameManager.instance.UseCard(DevelopmentCardType.Knight);
    }

    public void PlayRoadBuilding()
    {
        GameManager.instance.UseCard(DevelopmentCardType.RoadBuilding);
    }

    public void BuyFromShop(ShopManager shopManager, int item)
    {
        Destroy(shopManager.gameObject);
        flyCamera.enabled = true;
        switch (item)
        {
            case 0:
                // CALL: build road
                GameManager.instance.DisableEnableUIActions();
                GameManager.instance.neededBuildingType = BuildingTypeName.Road;
                GameManager.instance.neededBuildingNumber = 1;
                GameManager.instance.ChangeState(new Building(GameManager.instance));
                break;
            case 1:
                GameManager.instance.DisableEnableUIActions();
                GameManager.instance.neededBuildingType = BuildingTypeName.Settlement;
                GameManager.instance.neededBuildingNumber = 1;
                GameManager.instance.ChangeState(new Building(GameManager.instance));
                break;
            case 2:
                GameManager.instance.DisableEnableUIActions();
                GameManager.instance.neededBuildingType = BuildingTypeName.City;
                GameManager.instance.neededBuildingNumber = 1;
                GameManager.instance.ChangeState(new Building(GameManager.instance));
                break;
            case 3:
                GameManager.instance.HandleBuyDevelopmentCard();
                break;
        }
    }
}
