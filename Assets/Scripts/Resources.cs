using System.Collections;
using System.Collections.Generic;
using StateMachine;
using Unity.VisualScripting;
using UnityEngine;

public enum ResourceCardType
{
    Brick,
    Grain,
    Lumber,
    Ore,
    Wool,
    Desert,
    None
}

public enum DevelopmentCardType
{
    Knight,
    VictoryPoint,
    Monopoly,
    RoadBuilding,
    YearOfPlenty
}

public enum BuildingTypeName
{
    Road,
    Settlement,
    City,
    None
    
}

public abstract class BuildableObject
{
    public Dictionary<ResourceCardType, int> requirements = new();
    public int givesPoints;
    private BuildingTypeName buildingType;
    public BuildingTypeName BuildingType { get { return buildingType; } set { buildingType = value; } }

    public abstract void build(Player player, Object buildingLocation, bool firstTurn = false);
}

public class Road : BuildableObject
{
    public Road()
    {
        requirements.Add(ResourceCardType.Lumber, 1);
        requirements.Add(ResourceCardType.Brick, 1);
        BuildingType = BuildingTypeName.Road;
        
        givesPoints = 0;
    }

    public override void build(Player player, Object buildingLocation , bool firstTurn = false)
    {
        if (buildingLocation == null)
        {
            //SHOW SOME CONSOLE ERROR
            return;
        }
        // TO DO: EXPLICITLY CAST BUILDINGLOCATION TO HEX SIDE CLASS
        Border border = (Border)buildingLocation;
        if (!firstTurn)
        {
            foreach (var pair in requirements)
            {
                player.resources[pair.Key] -= pair.Value;
            }
        }
        player.points += givesPoints;
        player.roadList.Add(border);
        border.BuildRoad(player);
        // GameManager.instance.lastBuildRoad = border;
        
        // TO DO: DROP ROAD ON MAP
    }
}

public class Settlement : BuildableObject
{
    public Settlement()
    {
        requirements.Add(ResourceCardType.Lumber, 1);
        requirements.Add(ResourceCardType.Brick, 1);
        requirements.Add(ResourceCardType.Wool, 1);
        requirements.Add(ResourceCardType.Grain, 1);
        BuildingType = BuildingTypeName.Settlement;
        givesPoints = 1;
    }

    public override void build(Player player, Object buildingLocation , bool firstTurn = false)
    {
        // TO DO: CAST BUILDINGLOCATION TO CROSSROAD
        if (buildingLocation == null)
        {
            //SHOW SOME CONSOLE ERROR
            return;
        }
        Crossroad crossroad = (Crossroad)buildingLocation;
        foreach (var pair in requirements)
        {

            if (!firstTurn) player.resources[pair.Key] -= pair.Value;
        }
        player.points += givesPoints;

        crossroad.BuildBuilding(player, BuildingType);


        // GameManager.instance.lastBuildCrossroad = crossroad;

        // TO DO: DROP SETTLEMENT ON MAP
    }
}

public class City : BuildableObject
{
    public City()
    {
        requirements.Add(ResourceCardType.Ore, 3);
        requirements.Add(ResourceCardType.Grain, 2);
        BuildingType = BuildingTypeName.City;
        givesPoints = 1;
    }

    public override void build(Player player, Object buildingLocation , bool firstTurn = false)
    {
        // TO DO: CAST BUILDINGLOCATION TO CROSSROAD
        if (buildingLocation == null)
        {
            //SHOW SOME CONSOLE ERROR
            return;
        }

        foreach (var pair in requirements)
        {
            if (!firstTurn) player.resources[pair.Key] -= pair.Value;
        }
        player.points += givesPoints;
        Crossroad crossroad = (Crossroad)buildingLocation;

        crossroad.BuildBuilding(player, BuildingType);


        // TO DO: DROP CITY AND REMOVE SETTLEMENT
    }
}

public class DevelopmentCard : BuildableObject
{
    public DevelopmentCard()
    {
        requirements.Add(ResourceCardType.Ore, 1);
        requirements.Add(ResourceCardType.Grain, 1);
        requirements.Add(ResourceCardType.Wool, 1);

        givesPoints = 0;
    }
    

    public override void build(Player player, Object buildingLocation, bool firstTurn = false)
    {
        if (StateMachine.GameManager.instance.DeckIsEmpty())
        {
            //SHOW SOME CONSOLE ERROR
            return;
        }

        foreach (var pair in requirements)
        {
            if (!firstTurn) player.resources[pair.Key] -= pair.Value;
            player.points += givesPoints;
        }

        // CHOOSE A DEVELOPMENT CARD
        DevelopmentCardType developmentCard = StateMachine.GameManager.instance.ChooseDevelopmentCardFromDeck();

        if (player.turnDelayedDevelopmentCards.ContainsKey(developmentCard))
        {
            player.turnDelayedDevelopmentCards[developmentCard]++;
        }
        else
        {
            player.turnDelayedDevelopmentCards.Add(developmentCard, 1);
        }

    }


}