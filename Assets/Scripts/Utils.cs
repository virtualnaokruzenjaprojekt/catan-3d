using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utils
{
    public static bool ShouldOpenDialog(Dictionary<DevelopmentCardType, int> developmentCards, DevelopmentCardType cardType)
    {
        if(developmentCards.ContainsKey(cardType))
        {
            if(developmentCards[cardType] > 0)
            {
                return true;
            }
        }

        return false;
    }

    public static void TranslateDevelopmentCards<K, V>(Dictionary<K, V> dict1, Dictionary<K, V> dict2)
    {
        foreach(var v in dict1)
        {
            if(!dict2.ContainsKey(v.Key))
            {
                dict2.Add(v.Key, v.Value);
            } else
            {
                dict2[v.Key] = v.Value;
            }
        }
    }
    public static bool CanBuyDevelopmentCard(Dictionary<DevelopmentCardType, int> devCardsInShop)
    {
        foreach(var devCards in devCardsInShop)
        {
            if (devCards.Value != 0)
                return true;
        }

        return false;
    }
    public static List<int> PickRandomIndexes(int n, int listLength)
    {
        if (n > listLength)
        {
            Debug.LogWarning("Cannot pick more indexes than available in the list.");
            return null;
        }

        List<int> allIndexes = new List<int>();
        for (int i = 0; i < listLength; i++)
        {
            allIndexes.Add(i);
        }

        // Shuffle the list using Fisher-Yates algorithm
        System.Random rng = new System.Random();
        int count = allIndexes.Count;
        while (count > 1)
        {
            count--;
            int randomIndex = rng.Next(count + 1);
            int temp = allIndexes[randomIndex];
            allIndexes[randomIndex] = allIndexes[count];
            allIndexes[count] = temp;
        }

        // Take the first n elements
        List<int> randomIndexes = allIndexes.GetRange(0, n);

        return randomIndexes;
    }
}
