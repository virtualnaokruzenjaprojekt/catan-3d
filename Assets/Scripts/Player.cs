using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;
using StateMachine;

public class Player : MonoBehaviour
{
    public Dictionary<ResourceCardType, int> resources = new();
    public Dictionary<DevelopmentCardType, int> availableDevelopmentCards = new();

    public Dictionary<DevelopmentCardType, int> turnDelayedDevelopmentCards = new();
    public Dictionary<DevelopmentCardType, int> usedDevelopmentCards = new();

    public int points = 0;
    public int roads = 0;
    public List<Border> roadList = new List<Border>();
    public int army = 0;

    private int lastDiceThrowNumber = 0;
    
    public new string name = "Player";
    private PlayerColor color;
    public int initialBuildingStep = 0;
    public int position = -1;
    public PlayerColor Color { get { return color; } set { color = value; } }

    public void AddPoint(int points = 1)
    {
        this.points += points;

        if(this.points >= 10)
        {
            //HANDLE END GAME 
        }
    }
    
    public void RemovePoints(int points = 1)
    {
        this.points -= points;
        if (points < 0)
        {
            this.points = 0;
            throw new System.Exception("Points less then 0");
        }
    }

    public bool CheckIfCanBuild(BuildableObject obj)
    {
        bool hasResourcesNeeded = true;

        foreach(var pair in obj.requirements)
        {
            if(resources[pair.Key] < pair.Value)
            {
                hasResourcesNeeded = false;
                break;
            }
        }

        return hasResourcesNeeded;
    }

    public bool TryBuild(BuildableObject obj, Object buildingLocation, bool firstTurn = false ) {
        if (firstTurn || CheckIfCanBuild(obj))
        {
            //BUILD OBJ
            obj.build(this, buildingLocation, firstTurn: firstTurn);
            GameManager.instance.lastClickedCrossroad = null;
            GameManager.instance.lastClickedRoad = null;

            GameManager.instance.neededBuildingNumber--;
        
            if (GameManager.instance.neededBuildingNumber <= 0)
            {
                GameManager.instance.neededBuildingType = BuildingTypeName.None;

            }
            return true;
        }
        GameManager.instance.lastClickedCrossroad = null;
        GameManager.instance.lastClickedRoad = null;
        return false;
    }

    public void OnTurnChange()
    {
        foreach(var pair in turnDelayedDevelopmentCards)
        {
            if (availableDevelopmentCards.ContainsKey(pair.Key)) availableDevelopmentCards[pair.Key]+= pair.Value;
            else availableDevelopmentCards.Add(pair.Key, pair.Value);

            turnDelayedDevelopmentCards.Remove(pair.Key);
        }
    }

    public void GetFromTrade(List<int> addToResources)
    {
        int brick = addToResources[((int)ResourceCardType.Brick)];
        int grain = addToResources[((int)ResourceCardType.Grain)];
        int lumber = addToResources[((int)ResourceCardType.Lumber)];
        int ore = addToResources[((int)ResourceCardType.Ore)];
        int wool = addToResources[((int)ResourceCardType.Wool)];

        resources[ResourceCardType.Brick] += brick;
        resources[ResourceCardType.Grain] += grain;
        resources[ResourceCardType.Lumber] += lumber;
        resources[ResourceCardType.Ore] += ore;
        resources[ResourceCardType.Wool] += wool;
    }

    public void TradeInResources(List<int> removeFromResources)
    {
        int brick = removeFromResources[((int)ResourceCardType.Brick)];
        int grain = removeFromResources[((int)ResourceCardType.Grain)];
        int lumber = removeFromResources[((int)ResourceCardType.Lumber)];
        int ore = removeFromResources[((int)ResourceCardType.Ore)];
        int wool = removeFromResources[((int)ResourceCardType.Wool)];

        resources[ResourceCardType.Brick] -= brick;
        resources[ResourceCardType.Grain] -= grain;
        resources[ResourceCardType.Lumber] -= lumber;
        resources[ResourceCardType.Ore] -= ore;
        resources[ResourceCardType.Wool] -= wool;
    }

    public void AddKnight()
    {
        army++;

        if(army > GameManager.instance.largestArmy)
        {
            GameManager.instance.NewLargestArmy(this, army);
        }
    }

    public void SetLastDiceThrowNumber(int diceThrowNumber)
    {
        this.lastDiceThrowNumber = diceThrowNumber;
    }
    
    public int GetLastDiceThrowNumber()
    {
        return this.lastDiceThrowNumber;
    }
}



