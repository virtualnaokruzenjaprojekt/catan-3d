﻿using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;

namespace DefaultNamespace
{
    public enum PlayerColor
    {
       RED, ORANGE, BLUE, WHITE
    }
}



public class ColorMapping : MonoBehaviour
{
    // Define a dictionary to map PlayerColor enum values to Unity Color
    private static Dictionary<PlayerColor, Color> colorMap = new Dictionary<PlayerColor, Color>
    {
        { PlayerColor.RED, Color.red },
        { PlayerColor.ORANGE, Color.green },
        { PlayerColor.BLUE, Color.blue },
        { PlayerColor.WHITE, Color.white }
    };
    

    // Method to get Unity Color based on PlayerColor
    public static Color GetUnityColor(PlayerColor playerColor)
    {
        // Use the dictionary to get the corresponding Unity Color
        if (colorMap.ContainsKey(playerColor))
        {
            return colorMap[playerColor];
        }

        // Return a default color if not found (you can modify this part based on your requirements)
        return Color.black;
    }
}