using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThiefMarker : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Rotate the thief marker around it's own y axis
        transform.Rotate(Vector3.up * 50 * Time.deltaTime, Space.Self);
    }
}
