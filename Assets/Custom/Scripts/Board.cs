using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.Mathematics;
using UnityEngine;

public class Board : MonoBehaviour
{
    public ArrayList hexagons = new ArrayList();
    private Queue<ResourceCardType> resourceQueue;

    public GameObject roadPrefab;
    public float hexRadius = 41.0f; // Radius of the hexagon
    public float roadOffset = 4.0f; // Distance from the center of the hexagon to place the road placeholder

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GiveResources(int number)
    {

    }

    public void InitalizeBoard()
    {
        RandomiseBoard();
        foreach (Transform child in transform)
        {
            if (child.CompareTag("Hex"))
            {
                hexagons.Add(child);
            }
        }
        SetupBoard();
    }

    public void SetupBoard()
    {
        foreach (Transform hexagon in hexagons)
        {
            var hex = hexagon.GetComponent<Hexagon>();
            //Set the land type of the hexagon
            if (hex.fieldNumber != 10)
            {
                hex.landType = resourceQueue.Dequeue();
            }
            hex.PlaceTile();

            if(hex.landType == ResourceCardType.Desert)
            {
                hex.SetRobber(true);
            }

            //For each crossroad in create a road placeholder between crossroand and other crossroad that has distance of hexRadius +- roadOffset
            foreach (GameObject crossroad in hex.crossroads)
            {
                crossroad.GetComponent<Crossroad>().hexagons.Add(hex);
                // hex.GetComponent<TextMeshPro>().text = hex.fieldNumber.ToString();
                foreach (GameObject otherCrossroad in hex.crossroads)
                {
                    if (crossroad != otherCrossroad)
                    {
                        Vector3 crossroadPosition = crossroad.transform.position;
                        Vector3 otherCrossroadPosition = otherCrossroad.transform.position;
                        //Only proceed if distance between crossroads is hexRadius +- roadOffset
                        double distance = Vector3.Distance(crossroadPosition, otherCrossroadPosition);
                        if (distance < hexRadius - roadOffset || distance > hexRadius + roadOffset)
                        {
                            continue;
                        }

                        //Also check if there is already a road between these crossroads by accesing the crossroad's borders and comparing crossroads in borders
                        bool roadExists = false;
                        foreach (Border border in crossroad.GetComponent<Crossroad>().borders)
                        {
                            if (border != null && (border.crossroads[0] == otherCrossroad.GetComponent<Crossroad>() || border.crossroads[1] == otherCrossroad.GetComponent<Crossroad>()))
                            {
                                roadExists = true;
                                break;
                            }
                        }
                        //If there is already a road, continue to the next otherCrossroad
                        if (roadExists)
                        {
                            continue;
                        }
                            
                        //Instantiate a road placeholder between crossroad and otherCrossroad, right in the middle
                        Vector3 roadPosition = (crossroadPosition + otherCrossroadPosition) / 2;
                        GameObject road = Instantiate(roadPrefab, roadPosition, Quaternion.identity);
                        //Rotate the road on y axis so it's parralel to the line connecting crossroad and otherCrossroad
                        road.transform.LookAt(otherCrossroadPosition);
                        road.transform.Rotate(0, 90, 0);
                        road.transform.parent = transform;
                        //Set the crossroads in the road
                        road.GetComponent<Border>().crossroads = new Crossroad[]{ crossroad.GetComponent<Crossroad>(), otherCrossroad.GetComponent<Crossroad>() };
                        //Add the road to the crossroads' borders
                        crossroad.GetComponent<Crossroad>().borders.Add(road.GetComponent<Border>());
                        otherCrossroad.GetComponent<Crossroad>().borders.Add(road.GetComponent<Border>());

                    }
                }
            }
            
        }
    }

    private void RandomiseBoard()
    {
        //Create a list of strings with all the resources
        List<ResourceCardType> resourceList = new List<ResourceCardType>
        {
            ResourceCardType.Brick, ResourceCardType.Brick, ResourceCardType.Brick,
            ResourceCardType.Lumber, ResourceCardType.Lumber, ResourceCardType.Lumber, ResourceCardType.Lumber,
            ResourceCardType.Wool, ResourceCardType.Wool, ResourceCardType.Wool, ResourceCardType.Wool,
            ResourceCardType.Grain, ResourceCardType.Grain, ResourceCardType.Grain, ResourceCardType.Grain,
            ResourceCardType.Ore, ResourceCardType.Ore, ResourceCardType.Ore
        };

        //Randomise the list of strings
        System.Random random = new System.Random();
        int n = resourceList.Count;
        while (n > 1)
        {
            n--;
            int k = random.Next(n + 1);
            (resourceList[k], resourceList[n]) = (resourceList[n], resourceList[k]);
        }

        resourceQueue = new Queue<ResourceCardType>(resourceList);
    }

    public List<Hexagon> GetHexagonFromDiceNumber(int diceNumber)
    {
        List<Hexagon> hexagonsResult = new List<Hexagon>();
        foreach (Transform hexagonObject in hexagons)
        {
            Hexagon hexagon = hexagonObject.GetComponent<Hexagon>();
            int hexagonNumberNormalized;
            if (hexagon.fieldNumber is 1 or 2)
            {
                hexagonNumberNormalized = 2;
            }
            else
            {
                hexagonNumberNormalized = hexagon.fieldNumber / 2;
            }
            if (hexagon.fieldNumber == diceNumber || hexagonNumberNormalized == diceNumber)
            {
                hexagonsResult.Add(hexagon);
            }
        }
        if (hexagonsResult.Count > 0)
        {
            return hexagonsResult;
        }
        Debug.LogError("Hexagon with dice number " + diceNumber + " not found.");
        return null;
    }
    
}
