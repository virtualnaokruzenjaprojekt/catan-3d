using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ResourceCounter : MonoBehaviour
{
    public int value;
    public GameObject text;
    // Start is called before the first frame update
    void Start()
    {
        SetValue(value);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetValue(int value)
    {
        //if last value was 0 activate all children
        if (this.value == 0)
        {
            foreach (Transform child in transform)
            {
                child.gameObject.SetActive(true);
            }
        }
        this.value = value;
        text.GetComponent<TextMeshProUGUI>().text = value.ToString();
        if (value == 0)
        {
            //deactivate all children
            foreach (Transform child in transform)
            {
                child.gameObject.SetActive(false);
            }
        }
    }
}
