using System;
using System.Buffers;
using System.Collections;
using System.Collections.Generic;
using StateMachine;
using UnityEngine;
using static UnityEngine.UI.GridLayoutGroup;


public class Crossroad : MonoBehaviour
{
    public bool isTaken;
    public BuildingTypeName buildingType;
    public List<Border> borders;
    public GameObject buildingPrefabInstance;
    public List<Hexagon> hexagons = new List<Hexagon>();

    public Player owner;

    // Start is called before the first frame update
    void Start()
    {
        isTaken = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (isTaken)
        {
            isTaken = false;
        }
    }
    
    private void OnMouseDown()
    {
        // Save the current system time when the object is clicked
        // Debug.Log( "Crossroad clicked");
        // GameManager.instance.lastClickedCrossroad = this;
        GameManager.instance.setLastClickedCrossroad(this);
        GameManager.instance.ChooseLocation();
    }

    public Tuple<ResourceCardType, int> GrabResource(int hexagonNumber)
    {
        foreach (Hexagon hex in hexagons)
        {
            if (hex.fieldNumber == hexagonNumber)
            {
                if (buildingType == BuildingTypeName.Settlement)
                {
                    return new Tuple<ResourceCardType, int>(hex.landType, 1);
                }
                if (buildingType == BuildingTypeName.City)
                {
                    return new Tuple<ResourceCardType, int>(hex.landType, 2);
                }
            }
        }
        
        return new Tuple<ResourceCardType, int>(ResourceCardType.None, 0);
    }

    public void BuildBuilding(Player player, BuildingTypeName buildingType)
    {
        isTaken = true;
    
        transform.GetComponent<MeshRenderer>().enabled = false;
        updateNeighbouringCrossroads();
        this.buildingType = buildingType;
        owner = player;
        // Debug.Log("Building road for player " + player.name +" with color " + player.Color + " and building type " + buildingType);

        this.buildingPrefabInstance = Instantiate(Resources.Load<GameObject>("Prefabs/" + buildingType), transform.position + new Vector3(0.0f, -2.0f, 0.0f), transform.rotation);
        buildingPrefabInstance.GetComponent<MeshRenderer>().material.color = ColorMapping.GetUnityColor(player.Color);
    }

    public void updateNeighbouringCrossroads()
    {
        foreach (Border border in borders)
        {
            if (border.crossroads[0] == this)
            {
                border.crossroads[1].TryGetComponent(out Crossroad crossroad);
                border.crossroads[1].GetComponent<MeshRenderer>().enabled = false;
                // border.crossroads[1].GetComponent<CapsuleCollider>().enabled = false;
            }
            else
            {
                border.crossroads[0].TryGetComponent(out Crossroad crossroad);
                border.crossroads[0].GetComponent<MeshRenderer>().enabled = false;
                // border.crossroads[0].GetComponent<CapsuleCollider>().enabled = false;
            }
        }
    }
}
