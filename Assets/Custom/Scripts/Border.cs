using System;
using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using StateMachine;
using Unity.VisualScripting;
using UnityEngine;

public class Border : MonoBehaviour
{
    public Crossroad[] crossroads;
    public bool hasRoad;
    public Player owner;
    public GameObject roadPrefabInstance;

    // Start is called before the first frame update
    void Start()
    {
        hasRoad = false;
    }

    // Update is called once per frame
    void Update()
    {
    }
    
    private void OnMouseDown()
    {
        // Debug.Log( "Border clicked");
        // Save the current system time when the object is clicked
        // GameManager.instance.lastClickedRoad = this;
        GameManager.instance.setLastClickedRoad(this);
        GameManager.instance.ChooseLocation();
    }

    public void BuildRoad(Player player) {
        hasRoad = true;
        transform.GetComponent<MeshRenderer>().enabled = false;
        // instantiate road prefab at border position
        owner = player;
        // Debug.Log("Building road for player " + player.name +" with color " + player.Color);
        this.roadPrefabInstance = Instantiate(Resources.Load<GameObject>("Prefabs/Road"), transform.position, transform.rotation);
        this.roadPrefabInstance.GetComponent<MeshRenderer>().material.color = ColorMapping.GetUnityColor(player.Color);
    }

}
