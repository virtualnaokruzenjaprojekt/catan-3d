using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using StateMachine;
using Unity.VisualScripting;
using UnityEngine;

// public enum LandType
// {
//     Wool,
//     Grain,
//     Lumber,
//     Brick,
//     Ore,
//     Desert,
//     None
// }

public class Hexagon : MonoBehaviour
{
    public GameObject[] crossroads;
    public ResourceCardType landType = ResourceCardType.None;
    public bool hasRobber;
    public int fieldNumber;
    public GameObject resourceCounter;
    public GameObject thiefMarker;


    // Start is called before the first frame update
    void Start()
    {
        hasRobber = false;
    }

    // Update is called once per frame
    void Update()
    {
    }
    
    private void OnMouseDown()
    {
        // Save the current system time when the object is clicked
        GameManager.instance.setLastClickedHexagon(this);
        GameManager.instance.ChooseLocation();
    }

    public void SetRobber(bool hasRobber)
    {
        //if new value is true and old value is false spawn thief marker
        if (hasRobber && !this.hasRobber)
        {
            SpawnThiefMarker();
            this.hasRobber = hasRobber;
        } else if (!hasRobber && this.hasRobber)
        {
            DespawnThiefMarker();
            this.hasRobber = hasRobber;
        }
    }

    public void GenerateResource()
    {
        //Set the value of the resource counter
        //resourceCounter.GetComponent<ResourceCounter>().SetValue(value);
    }

    public void PlaceTile()
    {
        Debug.Log("Setting up hexagon: " + Directory.GetCurrentDirectory());
        transform.GetComponent<MeshRenderer>().enabled = false;
        string path = "Prefabs/" + landType;
        GameObject prefab = Resources.Load<GameObject>(path);
        if (prefab != null)
        {
            Instantiate(prefab, transform.position, transform.rotation);
            SpawnResourceCounter();
        }
        else
        {
            Debug.LogError("Prefab not found at path: " + path);
        }
    }

    public void SpawnResourceCounter()
    {
        //Instantiate resource counter prefab at the center of the hexagon at a certain height
        Vector3 position = transform.position;
        position.y += 26.0f;
        resourceCounter = Instantiate(Resources.Load<GameObject>("Prefabs/ResourceCounter"), position, transform.rotation);
    }

    public void SpawnThiefMarker()
    {
        //Instantiate thief marker prefab at the center of the hexagon at a certain height
        Vector3 position = transform.position;
        position.y += 26.0f;
        position.z += 20.0f;
        thiefMarker = Instantiate(Resources.Load<GameObject>("Prefabs/Thief"), position, transform.rotation);
    }

    public void DespawnThiefMarker()
    {
        Destroy(thiefMarker);
    }
    
    public List<Player> GetPlayersWithCrossroads()
    {
        List<Player> players = new List<Player>();
        foreach (GameObject crossroad in crossroads)
        {
            Crossroad crossroadScript = crossroad.GetComponent<Crossroad>();
            if (crossroadScript.isTaken)
            {
                players.Add(crossroadScript.owner);
            }
        }

        return players;
    }
}
